#!/bin/bash


# pc_inventory.sh --
#
#   This file permits to install fusioninventory to the local PC.
#
#   Created on 2010-21 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 9/10 XFCE and Xubuntu 14.04
#
#   Home web site : http://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

########################################################################################################

if [[ $(uname -a | grep -e "i686") ]] ; then
    echo "Version 32 bits"
    arch=i386
elif [[ $(uname -a | grep -e "i586") ]] ; then
    echo "Version 32 bits"
    arch=i386
elif [[ $(uname -a | grep -e "i386") ]] ; then
    echo "Version 32 bits"
    arch=i386
else
    echo "Version 64 bits"
    arch=amd64
fi


if [[ $(uname -a | grep -e "Debian") ]] ; then
    echo "Debian version"
    version=Debian
elif [[ $(uname -a | grep -e "Ubuntu") ]] ; then
    echo "Ubuntu version"
    version=Ubuntu
else
    echo "# OS version not found"
fi


if [[ ${version} = "Debian" ]] ; then
    cd Debian
    dpkg -i hwdata_*.deb
    dpkg -i libfile-which-perl_*.deb libhttp-daemon-perl_*.deb libnet-cups-perl_*${arch}.deb libparse-edid-perl_*.deb \
    libproc-daemon-perl_*.deb libsocket-getaddrinfo-perl_*.deb libtext-template-perl_*.deb libuniversal-require-perl_*.deb \
    libxml-treepp-perl_*.deb libyaml-perl_*.deb
    dpkg -i fusioninventory-agent_*.deb
    cd ..

elif [[ ${version} = "Ubuntu" ]] ; then
    cd Ubuntu_14.04
    dpkg -i libcommon-sense-perl_*${arch}.deb libfile-which-perl_*.deb libjson-perl_*.deb libjson-xs-perl_*${arch}.deb \
    libnet-cups-perl_*${arch}.deb libparse-edid-perl_*.deb libproc-pid-file-perl_*.deb libproc-processtable-perl_*${arch}.deb \
    libtext-template-perl_*.deb libuniversal-require-perl_*.deb libyaml-libyaml-perl_*${arch}.deb libyaml-perl_*.deb \
    libproc-daemon-perl_*.deb libsocket-getaddrinfo-perl_*${arch}.deb libxml-treepp-perl_*.deb
    dpkg -i fusioninventory-agent_*${arch}.deb
    cd ..
fi








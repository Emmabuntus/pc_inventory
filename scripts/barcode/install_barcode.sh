#!/bin/bash


# barcode.sh --
#
#   This file permits to install barcode to the local PC.
#
#   Created on 2010-21 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 9/10 XFCE and Xubuntu 14.04
#
#   Home web site : http://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

########################################################################################################

if [[ $(uname -a | grep -e "i686") ]] ; then
    echo "Version 32 bits"
    arch=i386
elif [[ $(uname -a | grep -e "i586") ]] ; then
    echo "Version 32 bits"
    arch=i386
elif [[ $(uname -a | grep -e "i386") ]] ; then
    echo "Version 32 bits"
    arch=i386
else
    echo "Version 64 bits"
    arch=amd64
fi


if [[ $(uname -a | grep -e "Debian") ]] ; then
    echo "Debian version"
    version=Debian
elif [[ $(uname -a | grep -e "Ubuntu") ]] ; then
    echo "Ubuntu version"
    version=Ubuntu
else
    echo "# OS version not found"
fi


if [[ ${version} = "Debian" ]] ; then
    cd Debian
    dpkg -i install-info_*${arch}.deb
    dpkg -i barcode_*${arch}.deb
    cd ..

elif [[ ${version} = "Ubuntu" ]] ; then
    cd Ubuntu_14.04
    dpkg -i barcode_*${arch}.deb
    cd ..
fi








#!/bin/bash


# pc_inventory_gui.sh --
#
#   This file permits to inventory the PC local and send information to server.
#
#   Created on 2010-21 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 10 XFCE.
#
#   Home web site : http://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

########################################################################################################

clear

name_barcode=barcode

if [[ $(uname -a | grep -e "Ubuntu") ]] ; then
    name_barcode=${name_barcode}.ps
    display_option="-monochrome"
else
    name_barcode=${name_barcode}.svg
    display_option=""
fi

home=$HOME
pwd=$(pwd)
barcode_local=${home}/.config/emmabuntus
name_inventory=inventory
inventory_local=${home}/${name_inventory}


# gestion de la configuration
nom_titre_window="PC Inventory"
config_inventory=./inventory_config

regenerate_inventory=0
display_codebar=1
touche_codebar=1
start_number=1
shutdown_pc=0
nb_inventory_files=0
serveur=""
protocol="http"

REGENERATE_INVENTORY_SENSITIVE=false

info_regenerate_inventory="Regénére l'inventaire"
info_display_codebar="Active la génération et l'affichage du code-barres"
info_touche_codebar="Mise en place de l'affichage du code-barres par Windows+a"
info_shutdown_pc="Active l'arrêt du PC à la fin de l'inventaire"
info_start_number="Numéro de démarrage des fichiers d'inventaire :  "
info_nb_inventory_files="Nombre de fichiers d'inventaire sur la clé USB :   "

comment_info_regenerate_inventory="Regénére l'inventaire même si le fichier ~/inventory.xml existe"
comment_info_display_codebar="Active la génération et l'affichage du code-barres"
comment_info_touche_codebar="Active la mise en place de l'affichage du code-barres suite à l'appui de la touche Windows+a"
comment_info_shutdown_pc=""
comment_info_start_number=""

    # Lecture fichier de config

    if [[ -f ${config_inventory} ]] ; then

        echo "# Config inventory found"
        source ${config_inventory}

    else

        echo "# Config inventory not found"
        zenity --error --text="Config inventory not found"
        exit 1

    fi

    # Config affichage

    if [[ ${regenerate_inventory} = "0" ]] ; then
        REGENERATE_INVENTORY=false
    else
        REGENERATE_INVENTORY=true
    fi

    if [[ ${display_codebar} = "0" ]] ; then
        DISPLAY_CODEBAR=false
    else
        DISPLAY_CODEBAR=true
    fi

    if [[ ${touche_codebar} = "0" ]] ; then
        TOUCHE_CODEBAR=false
    else
        TOUCHE_CODEBAR=true
    fi

    if [[ ${shutdown_pc} = "0" ]] ; then
        SHUTDOWN_PC=false
    else
        SHUTDOWN_PC=true
    fi


    START_NUMBER=${start_number}
    nb_inventory_files=$(ls ${pwd}/inventory/*.xml | wc -l)

    # Test si un fichier inventaire existe et dans ce cas affiche l'option comme active
    if [[ -f ${inventory_local}.xml ]] ; then

        REGENERATE_INVENTORY_SENSITIVE=true

    fi

    export WINDOW_DIALOG='<window title="'${nom_titre_window}'" icon-name="gtk-dialog-question" deletable="false" resizable="false" width_request="450"  height_request="260">
    <vbox spacing="5" space-fill="true" space-expand="true">

    <checkbox height_request="30" yalign="1" active="'${REGENERATE_INVENTORY}'" sensitive="'${REGENERATE_INVENTORY_SENSITIVE}'" tooltip-text="'${comment_info_regenerate_inventory}'">
    <variable>REGENERATE_INVENTORY</variable>
    <label>"'${info_regenerate_inventory}'"</label>
    </checkbox>

    <checkbox height_request="30" yalign="1" active="'${DISPLAY_CODEBAR}'" tooltip-text="'${comment_info_display_codebar}'">
    <variable>DISPLAY_CODEBAR</variable>
    <label>"'${info_display_codebar}'"</label>
    </checkbox>

    <checkbox height_request="30" yalign="1" active="'${TOUCHE_CODEBAR}'" tooltip-text="'${comment_info_touche_codebar}'">
    <variable>TOUCHE_CODEBAR</variable>
    <label>"'${info_touche_codebar}'"</label>
    </checkbox>

    <checkbox height_request="30" yalign="1" active="'${SHUTDOWN_PC}'" tooltip-text="'${comment_info_shutdown_pc}'">
    <variable>SHUTDOWN_PC</variable>
    <label>"'${info_shutdown_pc}'"</label>
    </checkbox>

    <hseparator space-expand="true" space-fill="true"></hseparator>

    <hbox>
    <text>
    <label>"'${info_start_number}'"</label>
    </text>
    <entry>
    <input>echo "'${START_NUMBER}'"</input>
    <variable>START_NUMBER</variable>
    </entry>
    </hbox>

    <hbox>
    <text>
    <label>"'${info_nb_inventory_files}'"</label>
    </text>
    <entry editable="false" width_chars="5">
    <input>echo "'${nb_inventory_files}'"</input>
    </entry>
    </hbox>

    <hseparator space-expand="true" space-fill="true"></hseparator>

    <hbox space-expand="false" space-fill="false">
    <button>
    <input file icon="gtk-no"></input>
    <label>"Annuler"</label>
    <action>exit:exit</action>
    </button>
    <button can-default="true" has-default="true" use-stock="true" is-focus="true">
    <label>gtk-ok</label>
    <action>exit:OK</action>
    </button>
    </hbox>
    </vbox>
    </window>'


    MENU_DIALOG="$(gtkdialog --center --program=WINDOW_DIALOG)"

    eval ${MENU_DIALOG}
    echo "MENU_DIALOG=${MENU_DIALOG}"


    # Mise à jour du fichier de config

    if [[ ${regenerate_inventory} = "0" && ${REGENERATE_INVENTORY} = true ]] ; then
        sed s/"regenerate_inventory=[^$]*"/"regenerate_inventory=1"/ ${config_inventory} > ${config_inventory}.tmp
        cp ${config_inventory}.tmp ${config_inventory}
        rm ${config_inventory}.tmp
    elif [[ ${regenerate_inventory} = "1" && ${REGENERATE_INVENTORY} = false ]] ; then
        sed s/"regenerate_inventory=[^$]*"/"regenerate_inventory=0"/ ${config_inventory} > ${config_inventory}.tmp
        cp ${config_inventory}.tmp ${config_inventory}
        rm ${config_inventory}.tmp
    fi

    if [[ ${display_codebar} = "0" && ${DISPLAY_CODEBAR} = true ]] ; then
        sed s/"display_codebar=[^$]*"/"display_codebar=1"/ ${config_inventory} > ${config_inventory}.tmp
        cp ${config_inventory}.tmp ${config_inventory}
        rm ${config_inventory}.tmp
    elif [[ ${display_codebar} = "1" && ${DISPLAY_CODEBAR} = false ]] ; then
        sed s/"display_codebar=[^$]*"/"display_codebar=0"/ ${config_inventory} > ${config_inventory}.tmp
        cp ${config_inventory}.tmp ${config_inventory}
        rm ${config_inventory}.tmp
    fi

    if [[ ${touche_codebar} = "0" && ${TOUCHE_CODEBAR} = true ]] ; then
        sed s/"touche_codebar=[^$]*"/"touche_codebar=1"/ ${config_inventory} > ${config_inventory}.tmp
        cp ${config_inventory}.tmp ${config_inventory}
        rm ${config_inventory}.tmp
    elif [[ ${touche_codebar} = "1" && ${TOUCHE_CODEBAR} = false ]] ; then
        sed s/"touche_codebar=[^$]*"/"touche_codebar=0"/ ${config_inventory} > ${config_inventory}.tmp
        cp ${config_inventory}.tmp ${config_inventory}
        rm ${config_inventory}.tmp
    fi

    if [[ ${shutdown_pc} = "0" && ${SHUTDOWN_PC} = true ]] ; then
        sed s/"shutdown_pc=[^$]*"/"shutdown_pc=1"/ ${config_inventory} > ${config_inventory}.tmp
        cp ${config_inventory}.tmp ${config_inventory}
        rm ${config_inventory}.tmp
    elif [[ ${shutdown_pc} = "1" && ${SHUTDOWN_PC} = false ]] ; then
        sed s/"shutdown_pc=[^$]*"/"shutdown_pc=0"/ ${config_inventory} > ${config_inventory}.tmp
        cp ${config_inventory}.tmp ${config_inventory}
        rm ${config_inventory}.tmp
    fi


    if [[ ${start_number} = ${START_NUMBER} ]] ; then
        echo "Nombre identique"
    else
        sed s/"start_number=[^$]*"/"start_number=${START_NUMBER}"/ ${config_inventory} > ${config_inventory}.tmp
        cp ${config_inventory}.tmp ${config_inventory}
        rm ${config_inventory}.tmp
    fi

    sed s/"error_msg=[^$]*"/"error_msg="/ ${config_inventory} > ${config_inventory}.tmp
    cp ${config_inventory}.tmp ${config_inventory}
    rm ${config_inventory}.tmp



if [ ${EXIT} == "OK" ] ; then

    # Vérification existance répertoire
    if [[ ! -d ${barcode_local} ]] ; then

        mkdir ${barcode_local}

    fi

    barcode_local=${barcode_local}/${name_barcode}

    (

        echo "# Start ..."
        pkexec ${pwd}/.pc_inventory.sh ${home} ${pwd} ${barcode_local}

    ) |
    zenity --progress \
      --title="Inventory PC" \
      --text="Start ..." \
      --width=500 \
      --percentage=0

    # Affichage erreur inventaire
    source ${config_inventory}

    if [[ ${error_msg} != "" ]]
    then
      zenity --error --text="${error_msg}" --width=500
    fi

    # Affichage du code-barre

    if [[ -f ${barcode_local} && ${DISPLAY_CODEBAR} = true ]] ; then

        display -border 50 -bordercolor white ${display_option} ${barcode_local} &

    fi

    # Installation affichage Code-barre enregistré suite à l'appui des touches Windows+A

    display_barcode_file=/usr/bin/display_barcode.sh

    if [[ -f ${display_barcode_file} ]] ; then

        if [[ ! $(xfconf-query --channel xfce4-keyboard-shortcuts --property "/commands/custom/<Super>a" | grep ${display_barcode_file}) ]] ; then
            xfconf-query --channel xfce4-keyboard-shortcuts --property "/commands/custom/<Super>a" --create --type string --set "${display_barcode_file}"
        fi

    fi

    # Arrêt du PC

    if [[ ${SHUTDOWN_PC} = true ]] ; then

        sleep 5
        xfce4-session-logout --halt

    fi

fi

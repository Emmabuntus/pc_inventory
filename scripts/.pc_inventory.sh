#!/bin/bash


# pc_inventory.sh --
#
#   This file permits to inventory the PC local and send information to server.
#
#   Created on 2010-21 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 10 XFCE.
#
#   Home web site : http://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

########################################################################################################

clear

dir_user=$1
dir_soft=$2
barcode_local=$3

config_inventory=${dir_soft}/inventory_config


if  [[ ${dir_user} == "" || ${dir_soft} == "" ]] ; then

    echo "10"
    echo -e "# Config pc_inventory is not correct need home directory and software directory"

    sed s/"error_msg=[^$]*"/"error_msg=\"Config pc_inventory is not correct need home directory and software directory\""/ ${config_inventory} > ${config_inventory}.tmp
    cp ${config_inventory}.tmp ${config_inventory}
    rm ${config_inventory}.tmp

    exit 1

fi

name_inventory=inventory
inventory_local=${dir_user}/${name_inventory}
nom_paquet=fusioninventory-agent
dir_inventory_software=${dir_soft}/fusioninventory-agent
install_inventory_software=install_fusioninventory_agent.sh
dir_sauvegarde_inventory_pc=${dir_soft}/inventory

pc_name=$(hostname)
date_inventory=$(date "+%Y-%m-%d-%H-%M-%S")
pc_name_id=${pc_name}_${date_inventory}


GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

# Lecture fichier de config

if [[ -f ${config_inventory} ]] ; then

    echo "10"
    echo "# Config inventory found"
    source ${config_inventory}

else

    echo "10"
    echo "# Config inventory not found"

fi

if [[ ${regenerate_inventory} = "1"  ]] ; then

    if [[ -f ${inventory_local}.xml ]] ; then
        echo "10" ; sleep 1
        echo "# Remove inventory file"
        rm ${inventory_local}.xml
    fi

fi


if [[ ! -f ${inventory_local}.xml ]] ; then

    echo "10" ; sleep 1
    echo "# Inventory file not found"

    # Test présence logiciel fusioninventory

    if [[ $(dpkg --get-selections ${nom_paquet} | grep -e "\<install\>") ]];  then

        echo "20" ; sleep 1
        echo "# Inventory software found"

    else

        echo "20" ; sleep 1
        echo "# Inventory software install"
        cd ${dir_inventory_software}
        sudo ./${install_inventory_software}
        cd ..

    fi

    # Réalisation de l'inventaire en local

    if [[ $(dpkg --get-selections ${nom_paquet} | grep -e "\<install\>") ]];  then

        echo "70"
        echo -e "# Inventory generate on local see : ${inventory_local}.xml"

        sudo fusioninventory-inventory > ${inventory_local}.xml
        sudo chmod a+rw ${inventory_local}.xml

        # Patch nom du  PC car celui-ci n'est pas pris en compte dans GLPI
        sed s/"<NAME>${pc_name}<\/NAME>"/"<NAME>${pc_name_id}<\/NAME>"/ ${inventory_local}.xml > ${inventory_local}.tmp.xml
        cp ${inventory_local}.tmp.xml ${inventory_local}.xml
        rm ${inventory_local}.tmp.xml

    else

        echo "100"
        echo -e "# Fusioninventory-agent not installed and not inventory availaible"

        sed s/"error_msg=[^$]*"/"error_msg=\"Fusioninventory-agent not installed and not inventory availaible\""/ ${config_inventory} > ${config_inventory}.tmp
        cp ${config_inventory}.tmp ${config_inventory}
        rm ${config_inventory}.tmp

        exit 1

    fi


    # Cherche une clé USB branchée et copie la fichier xml sur la clé USB en incrémentant le nombre dans le nom du fichier
    # puis renomme le fichier dans le home avec ce nom

    if [[ -f ${inventory_local}.xml ]] ; then

        if [[ -d ${dir_sauvegarde_inventory_pc} ]] ; then

            list=$(ls ${dir_sauvegarde_inventory_pc}/${name_inventory}*.xml | sort -rV)

            if [[ ${list} == "" ]] ; then

                if [[ ${start_number} != "" ]] ; then
                    dst_file=${name_inventory}_${start_number}.xml
                else
                    dst_file=${name_inventory}_1.xml
                fi

            else

                if [[ ${start_number} != "" ]] ; then
                    dst_file=${name_inventory}_${start_number}.xml
                else
                    dst_file=${name_inventory}_1.xml
                fi

                if [[ -f ${dir_sauvegarde_inventory_pc}/${dst_file} ]] ; then

                    echo "# Found number of inventory file on ${dir_sauvegarde_inventory_pc}"
                    max_file=$(echo ${list} |cut -d ' ' -f1)
                    max_number=$(awk -F awk -F "/${name_inventory}_" '{print $2}' <<< ${max_file} |cut -d '.' -f1)
                    max_number=$((${max_number} + 1))
                    dst_file=${name_inventory}_${max_number}.xml

                fi

            fi

            cp ${inventory_local}.xml ${dir_sauvegarde_inventory_pc}/${dst_file}
            cp ${inventory_local}.xml ${dir_user}/${dst_file}
            chmod a+rw ${dir_sauvegarde_inventory_pc}/${dst_file}
            chmod a+rw ${dir_user}/${dst_file}

            # Test inventaire correct

            if [[ $(cat ${dir_sauvegarde_inventory_pc}/${dst_file} | grep "<QUERY>INVENTORY</QUERY>") ]] ; then
                echo "# Inventory file write on ${dir_sauvegarde_inventory_pc}/${dst_file}"
            else
                echo "# Inventory file not write on ${dir_sauvegarde_inventory_pc}/${dst_file} !!!"

                sed s/"error_msg=[^$]*"/"error_msg=\"Inventory file not write on ${dst_file} !!!\""/ ${config_inventory} > ${config_inventory}.tmp
                cp ${config_inventory}.tmp ${config_inventory}
                rm ${config_inventory}.tmp

            fi

        fi

     else

        echo "100"
        echo "# Inventory file not generated on : ${inventory_local}.xml !!!"

        sed s/"error_msg=[^$]*"/"error_msg=\"Inventory file not generated !!!\""/ ${config_inventory} > ${config_inventory}.tmp
        cp ${config_inventory}.tmp ${config_inventory}
        rm ${config_inventory}.tmp

    fi

    # Test présence Internet

    if [[ ${serveur} != "" ]]  ; then

        if [[ $(ping -w 1 sourceforge.net | grep "1 received") ]] ; then

            echo "# Internet is a live"

            if [[ ${serveur} != "" && ${protocol} != "" ]] ; then

                echo "# Send inventory file to server"

                retour_serveur=$(fusioninventory-injector -v -f ${inventory_local}.xml --url ${protocol}://${serveur}/plugins/fusioninventory/)

                if [[ $(echo ${retour_serveur} | grep "OK") ]] ; then

                    echo "100"
                    echo -e "# Send inventory file to server is correct"

                else

                    echo "100"
                    echo "# Send inventory file to server is not correct"

                fi

            else

                echo "100"
                echo "# Config server not correct"

            fi

        else

            echo "100"
            echo "# Internet is not a live"

        fi

    fi

else

    echo "100"
    echo "# Inventory file found : ${inventory_local}.xml"

fi

# Gestion du code-barres

if [[ ${display_codebar} = "1"  ]] ; then

    nom_paquet=barcode
    dir_barcode_software=${dir_soft}/barcode
    install_barcode_software=install_barcode.sh

    if [[ $(dpkg --get-selections ${nom_paquet} | grep -e "\<install\>") ]];  then

        echo "Barcode software found"

    else

        echo "Barcode software install"
        cd ${dir_barcode_software}
        sudo ./${install_barcode_software}
        cd ..

    fi

    if [[ $(dpkg --get-selections ${nom_paquet} | grep -e "\<install\>") ]];  then

        UUID=$(cat ${inventory_local}.xml | grep "<UUID>")
        UUID=$(awk -F awk -F "<UUID>" '{print $2}' <<< ${UUID})
        UUID=$(awk -F awk -F "</UUID>" '{print $1}' <<< ${UUID})

        if [[ ${UUID} != "" ]];  then

            if [[ $(uname -a | grep -e "Ubuntu") ]] ; then
                barcode -E -b ${UUID} -o ${barcode_local}
                dst_file=${name_inventory}_${max_number}_${UUID}.ps
            else
                barcode -S -b ${UUID} -o ${barcode_local}
                dst_file=${name_inventory}_${max_number}_${UUID}.svg
            fi

            chmod a+rw ${barcode_local}

            cp ${barcode_local} ${dir_sauvegarde_inventory_pc}/${dst_file}
            chmod a+rw ${dir_sauvegarde_inventory_pc}/${dst_file}

        fi

    fi

fi

# Installation affichage code-barres enregistré suite à l'appui des touches Windows+A

display_barcode_file=/usr/bin/display_barcode.sh

if [[ ${touche_codebar} = "1"  ]] ; then

    if [[ ! -f ${display_barcode_file} ]] ; then

        cp ${dir_soft}/.display_barcode.sh ${display_barcode_file}
        chmod a+rX ${display_barcode_file}

    fi

fi


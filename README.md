# Présentation

Le but de ce script est de permettre la réalisation facile des inventaires de PC sur [Emmabuntüs Debian Edition](https://emmabuntus.org/la-distribution/) 1, 2, 3 basées sur Debian 8, 9, 10 et Emmabuntüs 3 basée sur Xubuntu 14.04. Il n’a pas besoin d’Internet pour s’exécuter, car les logiciels nécessaires sont inclus et installés par le script avant de faire l’inventaire. Ce script utilise le logiciel libre [FusionInventory-agent](https://fusioninventory.org/) pour faire l’inventaire qui sera ensuite chargé manuellement sur un serveur **[GLPI](https://fr.wikipedia.org/wiki/Gestionnaire_Libre_de_Parc_Informatique)**.

Remarque : le script peut envoyer directement via Internet l’inventaire du PC sur le serveur GLPI, mais n’arrivant pas pour l’instant à [configurer le serveur pour interdire les envois sans l’utilisation du mot de passe](http://fusioninventory.org/documentation/security.html), cela ne nous permet pas d’utiliser cette fonctionnalité. Ce problème de sécurité doit être bloqué sur le serveur GLPI en renommant le fichier <base_installation_GLPI>/plugins/fusioninventory/front/communication.php, voir ce conseil [ici](https://github.com/fusioninventory/fusioninventory-for-glpi/issues/3170).

# Utilisation

Copiez le contenu du répertoire scripts sur une clé USB.

Mettre en route un PC sous Emmabuntüs, puis insérez la clé USB, et double cliquez sur le lanceur Inventory_PC.

Le script va s'éxécuter et déposer les fichiers d'inventaires dans le dossier inventory. Pour plus d'informations, voir les tutoriels :

- [Tutoriel d'utilisation du script d'inventaire des PC](https://framagit.org/Emmabuntus/pc_inventory/-/blob/main/scripts/Utilisation_script_inventaire_PC.pdf)
- [Tutoriel de configuration de GLPI pour l'inventaire des PC](https://framagit.org/Emmabuntus/pc_inventory/-/blob/main/scripts/Configuration_GLPI_inventaire_PC.pdf)


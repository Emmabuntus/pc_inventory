#!/bin/bash


# pc_inventory_gui.sh --
#
#   This file permits to inventory the PC local and send information to server.
#
#   Created on 2010-21 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 10 XFCE.
#
#   Home web site : http://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

########################################################################################################

clear

name_barcode=barcode

if [[ $(uname -a | grep -e "Ubuntu") ]] ; then
    name_barcode=${name_barcode}.ps
    display_option="-monochrome"
else
    name_barcode=${name_barcode}.svg
    display_option=""
fi

home=$HOME
barcode_local=${home}/.config/emmabuntus/${name_barcode}

# Affichage du code-barre

if [[ -f ${barcode_local} ]] ; then

    display ${display_option} ${barcode_local} &

fi
